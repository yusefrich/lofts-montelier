<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    @include('layout._metas')

    <link rel="shortcut icon" href="{{ asset('assets_front/img/favicon.ico') }}">
    <link rel="stylesheet" href="{{asset('assets_front/css/app.css')}}?ver=12.0">
    <link rel="stylesheet" href="{{asset('assets_front/icons_font/css/style.css')}}?ver=12.0">
    <link
    rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.css"
  />
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-177593932-3"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-177593932-3');
    </script>

    @toastr_css
</head>

<body>
    <nav class="navbar montelier-menu navbar-expand-lg fixed-top navbar-dark bg-dark collapse justify-content-between py-0" id="navbarNav">
        <a class="navbar-brand" href="{{ route('index') }}">
            <img src="{{asset('assets_front/img/logo.png')}}" class="py-32" width="auto" height="auto" alt="" loading="lazy">
        </a>
        @yield('navigation')

        {{-- <ul class="navbar-nav text-center text-md-left mx-auto mx-md-0">
            <li class="nav-item active">
                <a class="nav-link text-light text-uppercase" onclick="collapseMenu('#navbarNav')" href="#exp360">Experiência 360</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-light text-uppercase" onclick="collapseMenu('#navbarNav')" href="#emp">empreendimento </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-light text-uppercase" onclick="collapseMenu('#navbarNav')" href="#subaASerra">Área de lazer</a>
            </li>
            <!-- <li class="nav-item">
                <a class="nav-link text-light text-uppercase" onclick="collapseMenu('#navbarNav')" href="#fotos">Fotos e vídeos</a>
            </li> -->
            <li class="nav-item">
                <a class="nav-link text-light text-uppercase" onclick="collapseMenu('#navbarNav')" href="#downloads">Book</a>
            </li>
            <!-- <li class="nav-item">
                <a class="nav-link text-light text-uppercase" onclick="collapseMenu('#navbarNav')" href="#downloads">Tabela de preços</a>
            </li> -->
            <!-- <li class="nav-item">
                <a class="nav-link text-light text-uppercase" onclick="collapseMenu('#navbarNav')" href="#obras">Acompanhe a obra</a>
            </li> -->
            <li class="nav-item">
                <a class="nav-link text-light text-uppercase" onclick="collapseMenu('#navbarNav')" href="#map-sec">Localização</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-light text-uppercase" onclick="collapseMenu('#navbarNav')" href="#buy">Compre online</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-light text-uppercase" onclick="collapseMenu('#navbarNav')" href="#contact">Contato</a>
            </li>
        </ul> --}}
    </nav>
    <button id="nav-toggler" data-aos="fade-down" class="btn btn-dark btn-top-menu" type="button" data-toggle="collapse" data-target="#navbarNav"
    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        {{-- <i class="icon icon-bars"></i> --}}
        <div id="nav-icon1">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </button>
    <main>
        @yield('content')
    </main>


    @stack('modais')

    @jquery
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>
    <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>

    <script src="{{asset('assets_front/js/knockout.js')}}">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.min.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-168658682-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-168658682-1');
    </script>
    {{-- <script src="https://storage.googleapis.com/vrview/2.0/build/vrview.min.js"></script> --}}
    <script>
        $('#nav-toggler').click(function(){
            $("#nav-icon1").toggleClass('open');
        });

        function collapseMenu(collapseId) {
            $(collapseId).collapse('toggle');   
            $("#nav-icon1").toggleClass('open');         
        }
    </script>
    @stack('scripts')

    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
      AOS.init();
    </script>
    {{-- <script>
            console.log("carousel being stoped");
            $('.carousel').each(function () {  
                $(this).carousel('pause');
            })
    </script> --}}
    
    @toastr_js
    @toastr_render

  
</body>

</html>
