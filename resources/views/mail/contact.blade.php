@component('mail::message')
<h4>Olá, {{$data->name}}</h4><br><br>

<p>Obrigado pelo seu interesse nos Lofts Montelier. Caso queira mais alguma informação entre em contato com nossa equipe http://lofts.condominiomontelier.com.br/#contact</p>

@endcomponent
