<!-- General CSS Files -->
<link rel="stylesheet" href="{{ asset('/assets/modules/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/modules/fontawesome/css/all.min.css') }}">

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dragula/3.7.2/dragula.css">

<!-- CSS Libraries -->

<!-- Template CSS -->
<link rel="stylesheet" href="{{ asset('/assets/css/style.css') }}?ver=3.0">
<link rel="stylesheet" href="{{ asset('assets_front/icons_font/css/style.css') }}?ver=3.0">
<link rel="stylesheet" href="{{ asset('/assets/css/components.css') }}?ver=3.0">
<link rel="stylesheet" href="{{ asset('/assets/css/custom.css') }}?ver=3.0">
<link rel="stylesheet" href="{{ asset('/assets/js-duallistbox/bootstrap-duallistbox.min.css') }}">
