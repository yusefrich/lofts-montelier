@php $year = isset($data) ? $data->year : date('Y') @endphp
<div class="col-sm-12 col-md-2">
  <div class="form-group mb-2">
    {!! Form::label('year', 'Ano', ['class' => '']) !!}
    {!! Form::number('year', $year, ['class' => 'form-control required']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-2">
  <div class="form-group mb-2">
    {!! Form::label('month', 'Mês', ['class' => '']) !!}
    {!! Form::select('month', [
      '01' => 'Janeiro',
      '02' => 'Fevereiro',
      '03' => 'Março',
      '04' => 'Abril',
      '05' => 'Maio',
      '06' => 'Junho',
      '07' => 'Julho',
      '08' => 'Agosto',
      '09' => 'Setembro',
      '10' => 'Outubro',
      '11' => 'Novembro',
      '12' => 'Dezembro',
    ], old('month'), ['class' => 'form-control required']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-8">
  <div class="form-group mb-8">
    {!! Form::label('img', isset($data) ? 'Imagem' : 'Imagens', ['class' => '']) !!}<br>
    @if(isset($data) && $data->img) <img src="{{url('storage/cons/'.$data->img)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    @if(isset($data))
      {!! Form::file('img') !!}
    @else
      <input multiple="multiple" name="imgs[]" type="file"> 
    @endif
  </div>
</div>

@if(isset($data))
<div class="col-sm-12 col-md-12">
  <div class="form-group mb-12">
    {!! Form::label('title', 'Descrição', ['class' => '']) !!}
    {!! Form::text('title', old('title'), ['class' => 'form-control required']) !!}
  </div>
</div>
@endif
