@php
  $file = isset($data) ? [] : ['required'];
@endphp
<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('name', 'Título', ['class' => '']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control', 'required']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('type', 'Tipo', ['class' => '']) !!}
    {!! Form::text('type', old('type'), ['class' => 'form-control', 'required']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('url', '360', ['class' => '']) !!}<br>
    {!! Form::text('url', old('url'), ['class' => 'form-control', 'required']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('img', 'Thumb', ['class' => '']) !!} <br>
    @if(isset($data) && $data->img) <img src="{{url('storage/home/'.$data->img)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    {!! Form::file('img', false) !!}
  </div>
</div>

