<div id="contact" class="container-large mx-auto pt-100 mb-100 pt-mdb-80 mb-mdb-120 px-3 overflow-hidden">
    <h1 data-aos="fade-up" data-aos-delay="300" class="text-dark text-uppercase mb-32">fale conosco agora</h1>
    <div class="btn-holder mb-100">
        <a style="font-size:13px;" target="_blank" href="{{$support->fc_1_url}}" data-aos="fade-right" data-aos-delay="200" class="btn my-2 btn-spacing mr-24 mr-mdb-0 mb-24 btn-outline-dark"> <span>{{$support->fc_1_txt}}</span> <i class="icon {{$support->fc_1_ico}} ml-16"></i></a>
        <a style="font-size:13px;" target="_blank" href="{{$support->fc_2_url}}" data-aos="fade-right" data-aos-delay="400" class="btn my-2 btn-spacing mr-24 mr-mdb-0 mb-24 btn-outline-dark"> <span>{{$support->fc_2_txt}}</span> <i class="icon  {{$support->fc_2_ico}} ml-16"></i></a>
        <a style="font-size:13px;" target="_blank" href="mailto:{{$support->fc_3_url}}" data-aos="fade-right" data-aos-delay="600" class="btn my-2 btn-spacing mr-24 mr-mdb-0 mb-24 btn-outline-dark"> <span>{{$support->fc_3_txt}}</span> <i class="icon  {{$support->fc_3_ico}} ml-16"></i></a>
        <a style="font-size:13px;" target="_blank" href="{{$support->fc_4_url}}" data-aos="fade-right" data-aos-delay="800" class="btn my-2 btn-spacing mr-24 mr-mdb-0 mb-24 btn-outline-dark"> <span>{{$support->fc_4_txt}}</span> <i class="icon  {{$support->fc_4_ico}} ml-16"></i></a>
    </div>

    <p data-aos="fade-left" data-aos-delay="300"  ><small class="text-dark mb-32 text-uppercase">{{$support->tl_txt}}</small></p>
    <div data-aos="fade-right" data-aos-delay="600"  class="d-flex justify-content-between">
        <p class="text-uppercase"><small>© 2020 ville blanche   |   <a class="link-mont text-dark" href="{{ route('terms') }}">Política de Privacidade</a></small></p>

        <a target="_blank" href="https://www.qualitare.com/" class="footer-by footer-qualitare d-none d-md-block">
            <img src="{{ asset('assets_front/img/qualitare.png') }}"  alt="">
        </a>
    </div>
    <a data-aos="fade-left" data-aos-delay="300" target="_blank" href="https://www.qualitare.com/" class="footer-by d-md-none float-right mt-32">
        <img src="{{ asset('assets_front/img/qualitare-old.png') }}"  alt="">
    </a>
</div>
