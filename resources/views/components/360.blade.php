<div  class="sidemenu_360">
    <ul class="pl-0 text-light d-none d-md-block">
        <li data-aos="fade-down">
            <img src="{{asset('assets_front/img/logo.png')}}" class="py-32" width="auto" height="auto" alt="" loading="lazy">
        </li>
        @php
            $subkeyNew = 0;
        @endphp
        @foreach($home1 as $key => $home)
        <div  data-aos="fade-up" class="position-relative mt-40">
            
            <div class="position-relative">
            <li class="text-uppercase sidebar-title">Lofts</li>
            </div>
            @foreach($home as $menu)
            <li style="display: none" data-aos="fade-right" data-aos-delay="300" class="text-uppercase my-16 my-mdb-8 text-left">
                <a data-url360="{{$menu->url}}" class="btn btn-block sidebar-link py-0 select-360 text-left @if($subkeyNew == 0) active @endif" onclick="setNew360('{{$menu->url}}', this, '{{$menu->name}}')">{{$menu->name}}</a>{{-- https://cors-anywhere.herokuapp.com/ --}}
                {{-- <a data-url360="{{url('storage/home/'.$menu->url)}}" class="btn btn-block sidebar-link py-0 select-360 text-left @if($subkey == 0) active @endif" onclick="serContentInfoCaller('{{url('storage/home/'.$menu->url)}}', this)">{{$menu->name}}</a> --}}{{-- https://cors-anywhere.herokuapp.com/ --}}
            </li>
            @php
                $subkeyNew ++;
            @endphp

            @endforeach
        </div>
        @endforeach
    </ul>
    <ul class="pl-0 text-light d-md-none">
        <li data-aos="fade-down">
            <img src="{{asset('assets_front/img/logo.png')}}" class="py-32" width="auto" height="auto" alt="" loading="lazy">
        </li>
        @php
            $subkeyNew = 0;
        @endphp
        <div style="display: none" data-aos="fade-up" class="position-relative mt-40">
            <li  class="text-uppercase sidebar-title" >SELECIONE O AMBIENTE</li>
            <li  class="text-uppercase my-16 my-mdb-8 text-left" data-toggle="collapse" data-target="#collapse360">
                <a  style="margin-left: 32px; max-width: 157px;" class="btn btn-block py-0 select-360 text-left pl-0 d-flex justify-content-between" ><span id="selectedMobile">selecionado</span> <ion-icon class="" name="chevron-down-outline"></ion-icon></a>{{-- https://cors-anywhere.herokuapp.com/ --}}
            </li>
            <div  id="collapse360" class="collapse">{{-- collapse --}}
                @foreach($home1 as $key => $home)
                @foreach($home as $menu)
                <li data-aos="fade-right" data-aos-delay="300" class=" text-uppercase my-16 my-mdb-8 text-left">
                    <a data-toggle="collapse" data-target="#collapse360" data-url360="{{$menu->url}}" class="btn btn-block sidebar-link py-0 select-360 text-left @if($subkeyNew == 0) active @endif" onclick="setNew360('{{$menu->url}}', this, '{{$menu->name}}')">{{$menu->name}}</a>{{-- https://cors-anywhere.herokuapp.com/ --}}
                    {{-- <a data-url360="{{url('storage/home/'.$menu->url)}}" class="btn btn-block sidebar-link py-0 select-360 text-left @if($subkey == 0) active @endif" onclick="serContentInfoCaller('{{url('storage/home/'.$menu->url)}}', this)">{{$menu->name}}</a> --}}{{-- https://cors-anywhere.herokuapp.com/ --}}
                </li>
                @php
                    $subkeyNew ++;
                @endphp
                @endforeach
                @endforeach
            </div>
        </div>
    </ul>
</div>

<div id="exp360" class="experiencia360 overflow-hidden">
    {{-- <div class="bg-fade"></div> --}}
    {{-- <div style="width: 100%; height: 100%;" id="vrview"></div> --}}
    <iframe class="iframe-hide" style="border:none;" width="100%" height="100%" id="iframe360" allowvr="yes" frameborder="0" allow="vr; xr; accelerometer; magnetometer; gyroscope; autoplay;" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" onmousewheel=""></iframe>
    {{-- <iframe class="iframe-hide" style="border:none;" width="100%" height="100%" id="iframe360" allowvr="yes" frameborder="0" allow="vr; xr; accelerometer; magnetometer; gyroscope; autoplay;" allowfullscreen></iframe> --}}
</div>
<div class="descubra-mais-holder d-flex justify-content-center ">
    <a style="box-shadow: none" href="#emp" class="btn p-0 border-0">
        <img class="position-absolute" src="{{asset('assets_front/img/descubra_mais.svg')}}" alt=""> 
        <small class="text-uppercase">
            descubra mais
        </small>
    </a>
</div>

@push('modais')
<div class="modal fade " id="modal_tutorial_360" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog vertical-center-modal">
      <div class="modal-content bg-light border-radius-0">
        <div class="modal-body text-center p-32 py-4  border-radius-0">
            <div class="row mx-0 px-3 px-md-0  mb-32">
                <div class="col-md-6 ">
                    <img class="mb-3" src="{{asset('assets_front/img/tuto_360_1.svg')}}" alt="">
                    <p class="text-dark">Clique na foto e arraste para visualizar o ambiente em 360°</p>
                </div>
                <div class="col-md-6">
                    <img class="mb-3" src="{{asset('assets_front/img/tuto_360_2.svg')}}" alt="">
                    <p class="text-dark">Clique na foto e arraste para visualizar o ambiente em 360°</p>
                </div>
            </div>
            <button type="button" class="btn btn-outline-dark text-uppercase" data-dismiss="modal" aria-label="Close">
                Entendi
            </button>
        </div>
      </div>
    </div>
  </div>
@endpush

@push('scripts')
    <script>
        function setNew360(url, caller, name) {
            console.log("setting new 360");
            console.log(url);
            /* vrView.setContentInfo({
                image: url,
            }); */
            $("#selectedMobile").html(name);
            $(".sidebar-link").each(function() {
                $(this).removeClass("active");
            })

            $(caller).addClass("active");

            var urlData = url.split("=");
            /* $('#iframe360').attr('src', "https://www.youtube.com/embed/"+ urlData[1] + "?autoplay=1&mute=1&loop=1&controls=0&showinfo=0&playlist="+ urlData[1]) */
            /* $('#iframe360').attr('src',  url + "/embed?chrome=min") */ /* loading with google vr view */
            console.log("teste de igframe de html do 360");
            console.log("{!!asset('assets_front/360/teste/index.html')!!}");
            $('#iframe360').attr('src', "{!!asset('assets_front/360/index.html')!!}");
        }
    </script>
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#modal_tutorial_360').modal('show');
        });
    </script>

    
    <script>
        /* window.addEventListener('load', onVrViewLoad('https://storage.googleapis.com/vrview/examples/coral.jpg')); */
        window.addEventListener('load', onVrViewLoadYoutube());
        //reload da imagem do 360
        var vrView;
        function serContentInfoCaller(url, caller) {
            $(".sidebar-link").each(function() {
                $(this).removeClass("active");
            })

            $(caller).addClass("active");
            console.log("vr view obj");
            console.log(caller);
            vrView.setContent({
                image: url,
            });
        }

        function onVrViewLoad(url) {
            var first360Url = $(".select-360").first();
            // Selector '#vrview' finds element with id 'vrview'.
            if(first360Url.length != 0){
                console.log("show")
                console.log(first360Url)
                console.log(first360Url.data( "url360" ))

                vrView = new VRView.Player('#vrview', {
                    image: first360Url.data( "url360" ),
                    /* image: "https://cors-anywhere.herokuapp.com/https://chales.condominiomontelier.com.br/storage/home/15913732070.jpeg", */
                    width: window.innerWidth,
                    height: window.innerHeight,
                });

            } else {
                vrView = new VRView.Player('#vrview', {
                    image: url,
                    width: window.innerWidth,
                    height: window.innerHeight,
                });
            }
        }
        function onVrViewLoadYoutube() {
            var first360Url = $(".select-360").first();
            // Selector '#vrview' finds element with id 'vrview'.
            if(first360Url.length != 0){
                console.log("show")
                console.log(first360Url)
                console.log(first360Url.data( "url360" ))

                setNew360(first360Url.data( "url360" ), first360Url, first360Url.html())

            }
        }

    </script>
@endpush

