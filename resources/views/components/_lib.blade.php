<div  class="position-relative overflow-hidden" >
    <div id="lib-obras-holder" class="img-lib d-flex hide-scrollbar" data-bind="foreach: images">
        <a data-toggle="modal"  data-bind="attr: {id: 'img-carousel-obras-'+$index()}" onclick="setCarouselModalObras(this)" class="pointer img-lib-hover transition" data-target="#img_obras">
            <img data-bind="attr: {src: url}" src="" class="lib-img-icon transition" alt="">
            <i class="icon icon-zoom center transition text-light"></i>
        </a>
        
    </div>
    <a onclick="scrollLiv('lib-obras-holder', 'left')" class="btn btn-dark btn-round btn-slide vertical-center btn-slider-left "  role="button" >
        <ion-icon class="slider-icon text-light" name="chevron-back-outline"></ion-icon>
    </a>
    <a onclick="scrollLiv('lib-obras-holder', 'right')" class="btn btn-dark btn-round btn-slide vertical-center btn-slider-right "  role="button" >
        <ion-icon class="slider-icon text-light" name="chevron-forward-outline"></ion-icon>
    </a>
</div>
@push('modais')
<div class="modal fade pr-0" id="img_obras" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-full">
        <div class="modal-content bg-transparent">
            <div class="modal-body px-0">
                <div class="d-flex justify-content-end">
                    <button type="button" class="btn btn-light btn-round p-3 mr-4" data-dismiss="modal" aria-label="Close">
                        <i class="icon icon-cross"></i>
                    </button>
                </div>
                <div style="width: 100vw" id="carousel_img_obras" class="carousel position-absolute slide vertical-center" data-ride="carousel">
                    <div data-bind="foreach: images" class="carousel-inner overflow-visible">
                        <div data-bind="css: { 'active' :  $index() == 0}" class="carousel-item px-200 img-carousel-lib">
                            <img  data-bind="attr: {src: url}" src="" class="d-block w-100 img-lib-modal" alt="">
                            <div class="carousel-caption d-none d-md-block img-desc">
                                <h4 data-bind="html: title" class="text-light lib-desc-offset text-uppercase"></h4>
                            </div>
                        </div>
                    </div>
                    <a class="btn btn-light btn-round btn-slide vertical-center btn-slider-left" href="#carousel_img_obras" role="button" data-slide="prev">
                        <ion-icon class="slider-icon" name="chevron-back-outline"></ion-icon>
                    </a>
                    <a class="btn btn-light btn-round btn-slide vertical-center btn-slider-right" href="#carousel_img_obras" role="button" data-slide="next">
                        <ion-icon class="slider-icon" name="chevron-forward-outline"></ion-icon>
                    </a>
                </div>
            </div>
            {{-- <div class="modal-footer border-0">
            </div> --}}
        </div>
    </div>
</div>
@endpush
