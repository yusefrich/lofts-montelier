<div id="map-sec" class="position-relative">
    <div class="d-none d-md-block" id="mapFixed" style="background-image: url('{{url('storage/support/'.$support->lc_pc)}}');"></div>
    {{-- <div id="map"></div> --}}
    <div class="container-large mx-auto px-3">
        <div class="map-info-holder">
            <h1 data-aos="fade-right" class="text-uppercase text-light mb-32">LOCALIZAÇÃO</h1>
            <p data-aos="fade-right" data-aos-delay="200" class="text-light mb-32">O Montelier é um condomínio único, localizado na melhor área de Bananeiras e
                conta com três acessos que oferecem diversas opções de turismo:</p>
            <div data-aos="fade-right" data-aos-delay="400" class="info-item">
                <h3 class="text-uppercase text-dark mb-2">BANANEIRAS</h3>
                <p class="text-dark">Conheça a antiga estação ferroviária e o Túnel da Serra da Viração e se aventure em uma trilha off-road.</p>
            </div>
            <div data-aos="fade-right" data-aos-delay="600" class="info-item">
                <h3 class="text-uppercase text-dark mb-2">Borborema</h3>
                <p class="text-dark">Passe pela Barragem de Canafístola II e vários engenhos e casarões históricos, contemplando belezas naturais em trilhas off-road.</p>
            </div>
            <div data-aos="fade-right" data-aos-delay="800" class="info-item mb-32">
                <h3 class="text-uppercase text-dark mb-2">Solânea</h3>
                <p class="text-dark">Aproveite a praticidade de um percurso urbanizado com diversas conveniências pelo
                    caminho.</p>
            </div>
            <a href="{{$support->lc_url}}" data-aos="fade-right" data-aos-delay="1000" target="_blank" class="btn btn-spacing btn-outline-light mt-32"><span>COMO CHEGAR</span> <i class="icon icon-plane ml-16"></i></a>
        </div>
    </div>
    <img class="d-md-none img-fluid" id="mapFixedMob" src="{{url('storage/support/'.$support->lc_mobile)}}" alt="">
</div>

@push('scripts')

<script>
    function initMap() {
        var mapStyles = [
            {
                "elementType": "geometry",
                "stylers": [
                {
                    "color": "#6B8857"
                }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                {
                    "color": "#523735"
                }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                {
                    "color": "#f5f1e6"
                }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                {
                    "color": "#c9b2a6"
                }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "geometry.stroke",
                "stylers": [
                {
                    "color": "#dcd2be"
                }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [
                {
                    "color": "#ae9e90"
                }
                ]
            },
            {
                "featureType": "landscape.natural",
                "elementType": "geometry",
                "stylers": [
                {
                    "color": "#6B8857"
                }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                {
                    "color": "#6B8857"
                }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text",
                "stylers": [
                {
                    "visibility": "off"
                }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [
                {
                    "color": "#93817c"
                }
                ]
            },
            {
                "featureType": "poi.business",
                "stylers": [
                {
                    "visibility": "off"
                }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry.fill",
                "stylers": [
                {
                    "color": "#a5b076"
                }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [
                {
                    "color": "#447530"
                }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                {
                    "color": "#6B8857"//roads shuld be whiiite
                }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                {
                    "visibility": "off"
                }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                {
                    "color": "#fdfcf8"
                }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                {
                    "color": "#566f45",
                }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                {
                    "color": "#566f45"
                }
                ]
            },
            {
                "featureType": "road.highway.controlled_access",
                "elementType": "geometry",
                "stylers": [
                {
                    "color": "#e98d58"
                }
                ]
            },
            {
                "featureType": "road.highway.controlled_access",
                "elementType": "geometry.stroke",
                "stylers": [
                {
                    "color": "#db8555"
                }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [
                {
                    "color": "#806b63"
                }
                ]
            },
            {
                "featureType": "transit",
                "stylers": [
                {
                    "visibility": "off"
                }
                ]
            },
            {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [
                {
                    "color": "#6B8857"
                }
                ]
            },
            {
                "featureType": "transit.line",
                "elementType": "labels.text.fill",
                "stylers": [
                {
                    "color": "#8f7d77"
                }
                ]
            },
            {
                "featureType": "transit.line",
                "elementType": "labels.text.stroke",
                "stylers": [
                {
                    "color": "#6B8857"
                }
                ]
            },
            {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [
                {
                    "color": "#6B8857"
                }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [
                {
                    "color": "#b9d3c2"
                }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                {
                    "color": "#92998d"
                }
                ]
            }
        ]
      // Styles a map in night mode.
        console.log("map being init");
        var mapEl = document.getElementById('map');
        /* var mapElMob = document.getElementById('mapMobile'); */
        var map;
        if(mapEl){
            map = new google.maps.Map(mapEl, {
                center: {lat: -6.771008, lng: -35.635364},
                zoom: 13,
                disableDefaultUI: true,
                gestureHandling: 'cooperative',
                styles: mapStyles
            });
            var marker = new google.maps.Marker({position: {lat: -6.771008, lng: -35.635364}, title: "Get", map: map});
        }
        /* if(mapElMob){
            map = new google.maps.Map(mapElMob, {
                center: {lat: -7.0942583, lng: -34.8257926},
                zoom: 16,
                disableDefaultUI: true,
                styles: mapStyles
            });
            var marker = new google.maps.Marker({position: {lat: -7.0942583, lng: -34.8396445}, title: "Get", map: map});
        } */




    }
</script>


<script src="https://maps.googleapis.com/maps/api/js?key={{env('MAPS_API_KEY')}}&callback=initMap" async defer></script>

@endpush
