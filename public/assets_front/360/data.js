var APP_DATA = {
  "scenes": [
    {
      "id": "0-fachada",
      "name": "Fachada",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1000,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -1.9554850360746237,
          "pitch": 0.2268883226540872,
          "rotation": 0,
          "target": "1-loft"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "1-loft",
      "name": "Loft",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1000,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.11881075482271264,
          "pitch": 0.20412012605961927,
          "rotation": 0,
          "target": "0-fachada"
        },
        {
          "yaw": 2.768153252305459,
          "pitch": -0.005943130477048086,
          "rotation": 0.7853981633974483,
          "target": "2-loft-mezanino"
        },
        {
          "yaw": 2.7733406920482997,
          "pitch": -0.144214408449443,
          "rotation": 5.497787143782138,
          "target": "3-estacionamento"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "2-loft-mezanino",
      "name": "Loft Mezanino",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1000,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 2.692481374017463,
          "pitch": 0.1913560693407632,
          "rotation": 3.9269908169872414,
          "target": "1-loft"
        },
        {
          "yaw": 2.696597314681487,
          "pitch": 0.005036637648956344,
          "rotation": 0,
          "target": "3-estacionamento"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "3-estacionamento",
      "name": "Estacionamento",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1000,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.6365235704582588,
          "pitch": 0.03255186764218365,
          "rotation": 0,
          "target": "1-loft"
        },
        {
          "yaw": -1.705257370354433,
          "pitch": 0.02564501949574627,
          "rotation": 5.497787143782138,
          "target": "4-pavimento-flats"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "4-pavimento-flats",
      "name": "Pavimento Flats",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1000,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -1.5540178898015675,
          "pitch": 0.11640924268222896,
          "rotation": 0,
          "target": "5-flat"
        },
        {
          "yaw": 1.6392086599781681,
          "pitch": 0.13427053344182482,
          "rotation": 0,
          "target": "5-flat"
        },
        {
          "yaw": 2.598953140919205,
          "pitch": 0.06759529063562653,
          "rotation": 0.7853981633974483,
          "target": "7-rooftop"
        },
        {
          "yaw": -2.4352963675619836,
          "pitch": 0.1267825625411767,
          "rotation": 3.9269908169872414,
          "target": "3-estacionamento"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "5-flat",
      "name": "Flat",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1000,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.05429680870144438,
          "pitch": 0.06862820137737025,
          "rotation": 0,
          "target": "6-flat-varanda"
        },
        {
          "yaw": 3.122017490522656,
          "pitch": 0.06391168455243701,
          "rotation": 0,
          "target": "4-pavimento-flats"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "6-flat-varanda",
      "name": "Flat Varanda",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1000,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -2.970290239985715,
          "pitch": 0.2276016542721937,
          "rotation": 0,
          "target": "5-flat"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "7-rooftop",
      "name": "RoofTop",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1000,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 2.5401064830764106,
          "pitch": 0.03473841340963446,
          "rotation": 3.141592653589793,
          "target": "4-pavimento-flats"
        },
        {
          "yaw": 1.79870102980696,
          "pitch": 0.04442267909432829,
          "rotation": 0,
          "target": "8-rooftop-b"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "8-rooftop-b",
      "name": "RoofTop B",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1000,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 1.7327731936164854,
          "pitch": 0.06955361129021398,
          "rotation": 0,
          "target": "7-rooftop"
        }
      ],
      "infoHotspots": []
    }
  ],
  "name": "Project Title",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": true,
    "fullscreenButton": false,
    "viewControlButtons": false
  }
};
